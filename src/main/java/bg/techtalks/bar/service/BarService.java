package bg.techtalks.bar.service;

import bg.techtalks.bar.model.Bar;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.UUID.randomUUID;

@Slf4j
@Service
public class BarService {

    public List<Bar> getAll() {

        List<Bar> bars = List.of(new Bar(randomUUID().toString(), "barq"), new Bar(randomUUID().toString(), "bart"));
        log.info("Getting all {}", bars);
        return bars;
    }
}
