package bg.techtalks.bar.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
public class Bar {
    String id;
    String name;
}
