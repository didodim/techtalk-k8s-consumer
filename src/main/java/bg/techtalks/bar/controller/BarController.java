package bg.techtalks.bar.controller;

import bg.techtalks.bar.model.Bar;
import bg.techtalks.bar.service.BarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("bar")
public class BarController {

    private BarService barService;

    @Autowired
    public BarController(BarService barService) {
        this.barService = barService;
    }

    @GetMapping
    public List<Bar> getAll() {
        return barService.getAll();

    }
}
