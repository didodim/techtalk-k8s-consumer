#!/usr/bin/env bash

tag=1.0.2
cd "$(dirname "$0")"
cd ..

echo "Building service"
sh gradlew clean build

echo "Build Docker Image on K8S"
docker build -t didodim/bar:latest -t didodim/bar:${tag} .

echo "Docker Push"
docker push didodim/bar:latest
docker push didodim/bar:${tag}

echo "Applying k8s deployments"
kubectl apply -f deployment
