FROM openjdk:9-jdk-slim
ADD build/libs/bar*.jar /opt/bar.jar
ENTRYPOINT ["java","-jar","/opt/bar.jar"]
